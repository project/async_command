package org.drupal.project.async_command;

/**
 * The Drupal instance that can be accessed locally.
 * Meaning that settings.php is accessible and Drush is executable to execute any drupal script.
 */
public class DLocalSite extends DSite {
}
