package org.drupal.project.async_command;

/**
 * Individual command to be executed. Each command is also registered with a druplet.
 * A command doesn't necessarily know a DrupalConnection. If needed, it can get from druplet.
 * The Record inner class needs to know a DrupalConnection in order to do database operations.
 *     f
 * Subclass can also write a initialize(...) function, which initialize parameters for the app and is used for CLI evaluation.
 */

public class DCommand {
}
