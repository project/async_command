package org.drupal.project.async_command;

/**
 * Database record for this AsyncCommand.
 * This is the "boundary object" between AsyncCommand and Druplet, where Druplet knows how to transfer data into the AsyncCommand,
 * And AsyncCommand only cares about the program logic.
 */

public class DRecord {
}
